> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicolaus Lopez

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)

#### README.md file should include the following items:

* Screenshot of AMPPS Instalation My PHP Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- initializes a git repository
2. git status- tells you about how your project is progressing in comparison to your repository 
3. git add- prepares the content to be committed
4. git commit- records the changes to the repository
5. git push- updates the remote repository 
6. git pull- fetch from a remote repository and bring locally 
7. git clone - create a working copy of a local repository 

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://confluence.atlassian.com/bitbucket/use-a-git-branch-to-merge-a-file-681902555.html "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/tutorials/tutorials.git.bitbucket.org "My Team Quotes Tutorial")
