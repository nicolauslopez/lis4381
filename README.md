> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicolaus Lopez

### [Assignment 1](a1/README.md):

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)

### [Assignment 2](a2/README.md); 

*Three Parts:*

1. Configure the XML how we want the layout to be
2. Write the java to switch to different layouts
3. Chapter Questions (Chs 3,4)

###  [Assignment 3](a3/README.md);

*Three Parts:*

1. Create Database to manipulate data
2. Create mobile app 
3. Chapter Questions (Chs 5,6)

### [Project 1](p1/README.md);

*Three Parts:*

1. Create Business card app
2. Research adding borders, launcher icons, and text shadows
3. Chapter Questions (Chs 7,8)

### [Assignment 4](a4/README.md);

*Three Parts:*

1. Cloned starter files
2. Create HTML form with validation
3. Chapter Questions (Chs 9,10, & 19)

### [Assignment 5](a5/README.md);

*Three Parts:*

1. Cloned starter files
2. Create create webpage with add insert functionality 
3. Chapter Questions (Chs 11,12)

### [Project 2](p2/README.md)

*Three Parts:*

1. Add delete functionality
2. Add edit functionality
3. Chapter Questions (Chs 13,14)