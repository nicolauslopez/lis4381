> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicolaus Lopez

### Assignment 4 Requirements:

*Three Parts:*

1. Cloned starter files
2. Create HTML form with validation
3. Chapter Questions (Chs 9,10, & 19)

#### README.md file should include the following items:
* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples;
* Screenshot of portal main page;
* Screenshot failed validation;
* Screenshot passed validation;


#### Assignment Screenshots:

*Screenshot of Main Page*;
![Main Page](img/mainpage.png)

*Screenshot of failed validation*;
![First User Interface](img/failedvalidation.png)

*Screenshot of passed validation*:

![Second User Interface](img/passedvalidation.png)

