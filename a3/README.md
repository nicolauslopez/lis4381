> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicolaus Lopez

### Assignment 3 Requirements:

*Three Parts:*

1. Create a very simple database 
2. Create a mobile app that tells you how much tickets for concerts are
3. Chapter Questions (Chs 5,6)

#### README.md file should include the following items:
* Course title, your name, assignment requirements, as per A1;
* Screenshot of ERD;
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;
* Links to the following files:
	* a3.mwb
	* a3.sql

#### Assignment Screenshots:

*Screenshot of ERD*;
![MySQL ERD](img/erd.png)

*Screenshot of application running first user interface*:

![First User Interface](img/app.png)

*Screenshot of application running second user interface*:

![Second User Interface](img/app2.png)

