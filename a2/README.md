> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicolaus Lopez

### Assignment 2 Requirements:

*Three Parts:*

1. Configure the XML how we want the layout to be
2. Write the java to switch to different layouts
3. Chapter Questions (Chs 3,4)

#### README.md file should include the following items:
* Course title, your name, assignment requirements, as per A1
* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface

#### Assignment Screenshots:

*Screenshot of application running first user interface*:

![First User Interface](img/layout1.png)

*Screenshot of application running second user interface*:

![Second User Interface](img/layout2.png)

