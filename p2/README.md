> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicolaus Lopez

### Assignment 5 Requirements:

*Three Parts:*

1. Add delete functionality
2. Add edit functionality
3. Chapter Questions (Chs 13,14)

#### README.md file should include the following items:
* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381/


#### Assignment Screenshots:

*Screenshot of index*;
![Main Page](img/index.png)

*Screenshot of edit petstore*;
![edit_petstore.php](img/edit_petstore.png)

*Screenshot of edit petstore process*;
![edit_petstore_process.php (that includes error.php)](img/edit_petstore_process.png)

*Screen shot of home page*;
![Carousel (home page - include other content, e.g., images)](img/carosel.png)

*Screenshot of RSS Feed*;
![RSS Feed (link to page of your choice)](img/rss_feed.png)



