> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicolaus Lopez

### Assignment 5 Requirements:

*Three Parts:*

1. Cloned starter files
2. Create create webpage with add insert functionality 
3. Chapter Questions (Chs 11,12)

#### README.md file should include the following items:
* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples;
* Screenshot of portal main page;
* Screenshot failed validation;



#### Assignment Screenshots:

*Screenshot of Main Page*;
![Main Page](img/index.png)

*Screenshot of failed validation*;
![First User Interface](img/error.png)



