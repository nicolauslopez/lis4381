> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicolaus Lopez

### Project 1 Requirements:

*Three Parts:*

1. Create Business card app
2. Research adding borders, launcher icons, and text shadows
3. Chapter Questions (Chs 7,8)

#### README.md file should include the following items:
* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;

#### Project Screenshots:

*Screenshot of application running first user interface*:

![First User Interface](img/app.png)

*Screenshot of application running second user interface*:

![Second User Interface](img/app2.png)

